import theme from "@components/utils/theme";

export const buttonStyle = {
  backgroundColor: "#454545",
  marginRight: 1,
  height: "30px",
};
