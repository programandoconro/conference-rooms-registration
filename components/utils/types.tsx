export type ReservationType = {
  company: string;
  name: string;
  email: string;
  date: string;
  hour: string;
  room: string;
  timestamp: string;
};
export type UserType = {
  company: string;
  name: string;
  email: string;
  password: string;
};
